
<!-- Please describe your changes here -->

----------------------------------------------------------------------------

- [ ] I have opened an issue where I discuss the change I wish to make.
- [ ] I ran `./gradlew -Pqc ktlintCheck`, `./gradlew -Pqc detekt` and
  `./gradlew :ultrasonic:lintRelease` and no problems found. See
  [CONTRIBUTING](CONTRIBUTING.md) for further information.
- [ ] I'm using my own branch in my local copy. Ej, I want to merge
  `myuser/ultrasonic:my-new-contribution` into `develop`.
- [ ] All commits [are
  signed](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/).
- [ ] I agree to release my code and all other changes of this MR under the
  [GNU GPLv3](LICENSE) license.
