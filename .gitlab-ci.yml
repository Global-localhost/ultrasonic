default:
  image: 	registry.gitlab.com/ultrasonic/ci-android:latest
  cache: &global_cache
    key:
      files:
        - gradle/wrapper/gradle-wrapper.properties
    paths:
      - .gradle/

variables:
  CACHE_FALLBACK_KEY: develop-protected
  MEMORY_CONFIG: "-Xmx3200m -Xms256m -XX:MaxMetaspaceSize=1g"
  MEMORY_CONFIG_DEBUG: "-Xmx3200m -Xms256m -XX:MaxMetaspaceSize=1g -verbose:gc -Xlog:gc*"
  JVM_OPTS: ${MEMORY_CONFIG}
  JAVA_TOOL_OPTIONS: ${MEMORY_CONFIG}
  GRADLE_OPTS: ${MEMORY_CONFIG}
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/Ultrasonic/${CI_COMMIT_TAG}"
  PACKAGE_APK: "ultrasonic-${CI_COMMIT_TAG}.apk"
  PACKAGE_APK_IDSIG: "ultrasonic-${CI_COMMIT_TAG}.apk.idsig"
  GRADLE_USER_HOME: "$CI_PROJECT_DIR/.gradle"
  # The project id of https://gitlab.com/ultrasonic/ultrasonic/
  ROOT_PROJECT_ID: 37671564

stages:
  - Check
  - Build
  - Sign APK
  - Publish
  - Release

Check Style:
  stage: Check
  script: ./gradlew -Pqc ktlintCheck
  cache:
    # inherit all global cache settings
    <<: *global_cache
    policy: pull
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop" || $CI_COMMIT_REF_NAME == "master" || $CI_COMMIT_TAG || $CI_PROJECT_ID != $ROOT_PROJECT_ID
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

Static Analysis:
  stage: Check
  script: ./gradlew -Pqc detekt
  cache:
    # inherit all global cache settings
    <<: *global_cache
    policy: pull
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop" || $CI_COMMIT_REF_NAME == "master" || $CI_COMMIT_TAG || $CI_PROJECT_ID != $ROOT_PROJECT_ID
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

Lint:
  stage: Check
  script: ./gradlew :ultrasonic:lintRelease
  cache:
    # inherit all global cache settings
    <<: *global_cache
    policy: pull-push
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop" || $CI_COMMIT_REF_NAME == "master" || $CI_COMMIT_TAG || $CI_PROJECT_ID != $ROOT_PROJECT_ID
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

Unit Tests:
  stage: Check
  script: ./gradlew ciTest testDebugUnitTest
  cache:
    # inherit all global cache settings
    <<: *global_cache
    policy: pull-push
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop" || $CI_COMMIT_REF_NAME == "master" || $CI_COMMIT_TAG || $CI_PROJECT_ID != $ROOT_PROJECT_ID
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

Assemble Release:
  stage: Build
  script: ./gradlew assembleRelease
  artifacts:
    name: ultrasonic-release-unsigned-${CI_COMMIT_SHA}
    paths:
      - ultrasonic/build/outputs/apk/release/ultrasonic-release-unsigned.apk
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop" || $CI_COMMIT_REF_NAME == "master" || $CI_COMMIT_TAG || $CI_PROJECT_ID != $ROOT_PROJECT_ID

Generate Signed Develop APK:
  stage: Sign APK
  # We don't need the gradle cache here
  cache: []
  script:
    - openssl aes-256-cbc -K ${ULTRASONIC_KEYSTORE_KEY} -iv ${ULTRASONIC_KEYSTORE_IV} -in ultrasonic-keystore.enc -out ultrasonic-keystore -d
    - mkdir -p ${CI_PROJECT_DIR}/ultrasonic-release
    - ${ANDROID_HOME}/build-tools/*/zipalign -v 4 ultrasonic/build/outputs/apk/release/ultrasonic-release-unsigned.apk ${CI_PROJECT_DIR}/ultrasonic-release/ultrasonic-${CI_COMMIT_SHA}.apk
    - ${ANDROID_HOME}/build-tools/*/apksigner sign --verbose --ks ${CI_PROJECT_DIR}/ultrasonic-keystore --ks-pass pass:${ULTRASONIC_KEYSTORE_STOREPASS} --key-pass pass:${ULTRASONIC_KEYSTORE_KEYPASS} ${CI_PROJECT_DIR}/ultrasonic-release/ultrasonic-${CI_COMMIT_SHA}.apk
    - ${ANDROID_HOME}/build-tools/*/apksigner verify --verbose ${CI_PROJECT_DIR}/ultrasonic-release/ultrasonic-${CI_COMMIT_SHA}.apk
  artifacts:
    name: ultrasonic-${CI_COMMIT_SHA}
    paths:
      - ultrasonic-release/
  rules:
    - if: $CI_COMMIT_REF_NAME == "develop" && $CI_PROJECT_ID == $ROOT_PROJECT_ID

Generate Signed APK:
  stage: Sign APK
  # We don't need the gradle cache here
  cache: []
  script:
    - openssl aes-256-cbc -K ${ULTRASONIC_KEYSTORE_KEY} -iv ${ULTRASONIC_KEYSTORE_IV} -in ultrasonic-keystore.enc -out ultrasonic-keystore -d
    - mkdir -p ${CI_PROJECT_DIR}/ultrasonic-release
    - ${ANDROID_HOME}/build-tools/*/zipalign -v 4 ultrasonic/build/outputs/apk/release/ultrasonic-release-unsigned.apk ${CI_PROJECT_DIR}/ultrasonic-release/${PACKAGE_APK}
    - ${ANDROID_HOME}/build-tools/*/apksigner sign --verbose --ks ${CI_PROJECT_DIR}/ultrasonic-keystore --ks-pass pass:${ULTRASONIC_KEYSTORE_STOREPASS} --key-pass pass:${ULTRASONIC_KEYSTORE_KEYPASS} ${CI_PROJECT_DIR}/ultrasonic-release/${PACKAGE_APK}
    - ${ANDROID_HOME}/build-tools/*/apksigner verify --verbose ${CI_PROJECT_DIR}/ultrasonic-release/${PACKAGE_APK}
  artifacts:
    name: ultrasonic-${CI_COMMIT_TAG}
    paths:
      - ultrasonic-release/
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_ID == $ROOT_PROJECT_ID

Publish Signed APK:
  stage: Publish
  image: curlimages/curl:latest
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ultrasonic-release/${PACKAGE_APK} "${PACKAGE_REGISTRY_URL}/${PACKAGE_APK}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ultrasonic-release/${PACKAGE_APK_IDSIG} "${PACKAGE_REGISTRY_URL}/${PACKAGE_APK_IDSIG}"
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_ID == $ROOT_PROJECT_ID

Release:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script: |
    release-cli create --name "Release ${CI_COMMIT_TAG}" --tag-name ${CI_COMMIT_TAG} \
      --assets-link "{\"name\":\"${PACKAGE_APK}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_APK}\"}" \
      --assets-link "{\"name\":\"${PACKAGE_APK_IDSIG}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_APK_IDSIG}\"}"
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_ID == $ROOT_PROJECT_ID
